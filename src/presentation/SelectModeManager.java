package presentation;

import java.awt.event.MouseEvent;

import control.ControlManager;
import presentation.MainFrame;
import presentation.MapPanel;

public abstract class SelectModeManager {
	// Il faudrait peut-être mettre cette classe abstraites et ses filles dans le package presentation.
	protected MainFrame main_frame;
	protected MapPanel mapPanel;
	protected ControlManager manager;
	
	public SelectModeManager(MainFrame main_frame) {
		this.main_frame = main_frame;
		mapPanel = main_frame.getMapPanel();
		manager = main_frame.getControlManager();
	}
	
	public abstract void handleMousePressed(MouseEvent e);
	
	public abstract void handleMouseDragged(MouseEvent e);
	
	public abstract String getMode();
}
