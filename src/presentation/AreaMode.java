package presentation;

import control.ControlManager;

import java.awt.Point;
import java.awt.event.MouseEvent;

public class AreaMode extends SelectModeManager {

	public AreaMode(MainFrame main_frame) {
		super(main_frame);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void handleMousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		Point hitPosition = new Point(e.getX(),e.getY());
		drawArea(hitPosition);
	}

	@Override
	public void handleMouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		Point hitPosition = new Point(e.getX(),e.getY());
		drawArea(hitPosition);
	}

	@Override
	public String getMode() {
		// TODO Auto-generated method stub
		return "area";
	}
	
	private void drawArea(Point p) {
		//MapPanel Aspect
		Point tile_coord = mapPanel.getTile(p);
		int lat = (int) tile_coord.getX();
		int lon = (int) tile_coord.getY();

		manager.update_area(lat,lon);
	}


}
