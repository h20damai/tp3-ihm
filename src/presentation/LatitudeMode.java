package presentation;

import java.awt.Point;
import java.awt.event.MouseEvent;

public class LatitudeMode extends SelectModeManager{

	public LatitudeMode(MainFrame main_frame) {
		super(main_frame);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void handleMousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

		main_frame.setLatitudeDrawn(true);

		Point hitPosition = new Point(e.getX(),e.getY());
		drawLine(hitPosition);
	}

	@Override
	public void handleMouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		Point hitPosition = new Point(e.getX(),e.getY());
		drawLine(hitPosition);
	}

	@Override
	public String getMode() {
		// TODO Auto-generated method stub
		return "latitude";
	}
	
	private void drawLine(Point p) {
		//MapPanel Aspect
		int lat = mapPanel.getLatitude(p);
		mapPanel.drawLine(lat);
		mapPanel.repaint();

		// HistPanel aspect
		manager.update_hist_data(lat,main_frame.getHistView().getCurrentYear());

	}

}
