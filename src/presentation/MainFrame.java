package presentation;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.awt.GridLayout;
import java.awt.event.*;

import control.ControlManager;

import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import javax.swing.ButtonGroup;

public class MainFrame {

	private JFrame frame;
	
	private final ButtonGroup buttonGroup = new ButtonGroup();
	
	// Components of the interface I need access to in Main.update()
	private JTextField txtYeartextfield;
	private JSlider years_slider;
	
	private MapPanel panel;
	private ControlManager controlManager;
	private GraphView graphView;
	private HistView histView;

	private boolean graphViewIsActive = false;
	private boolean histViewIsActive = false;

	//Enable us to forcely desactivate the apparition of histogramm or graph if no area or latitude is selected.
	private boolean isAreaSelected = false;
	private boolean isLatitudeDrawn = false;
	
	private SelectModeManager current_mode;
	private SelectModeManager latitude_mode;
	private SelectModeManager area_mode;



	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame window = new MainFrame();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainFrame() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1200, 650);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Data Vis' (Community edition)");

		
		//Load the DataManager
		controlManager = new ControlManager(this);
		controlManager.initialize_data();
		
		// Init the graphView
		graphView = new GraphView(this);
		graphView.setVisible(graphViewIsActive&&isAreaSelected);
		graphView.setSize(600,400);
		graphView.setLocationRelativeTo(null);

			// This listener enables us to tell the main_frame that the graphView is disabled when we close the window manually.
			// Besides, the close operation on the JDialog is set on hide the window.
		graphView.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentHidden(ComponentEvent e) {
				graphViewIsActive = false;
			}
		});

		// Init the histView
		histView = new HistView(this);
		histView.setVisible(histViewIsActive&&isLatitudeDrawn);
		histView.setSize(600,400);
		histView.setLocationRelativeTo(null);

		histView.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentHidden(ComponentEvent e) {
				histViewIsActive = false;
			}
		});

		
		// CenterPanel
		panel = new MapPanel("src/presentation/earth.png");
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		
		// Instanciates the modes
		latitude_mode = new LatitudeMode(this);
		area_mode = new AreaMode(this);
		current_mode = latitude_mode;
		
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				// draw the line on the Map
				current_mode.handleMousePressed(e); // voir comment les traiter, peut être avec un setter.
				// add later the update on the histogram.
				// TODO
			}
		});
		
		panel.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				current_mode.handleMouseDragged(e);
			}
		});
		
		
		
		// EastPanel
		JPanel panel_est = new JPanel();
		frame.getContentPane().add(panel_est, BorderLayout.EAST);
		panel_est.setLayout(new GridLayout(0, 1, 50, 10));
		
		JLabel lblNewLabel_1 = new JLabel("Selection mode :");
		panel_est.add(lblNewLabel_1);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Select area");
		buttonGroup.add(rdbtnNewRadioButton);
		panel_est.add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("Select latitude");
		buttonGroup.add(rdbtnNewRadioButton_1);
		panel_est.add(rdbtnNewRadioButton_1);
		
		rdbtnNewRadioButton.addActionListener(e -> {
			setMode(area_mode);
		});
		
		rdbtnNewRadioButton_1.addActionListener(e ->{
			setMode(latitude_mode);
		});
		rdbtnNewRadioButton_1.setSelected(true);
		
		
		JLabel lblNewLabel_2 = new JLabel("Displaying mode :");
		panel_est.add(lblNewLabel_2);
		
		JButton btnNewButton = new JButton("Print Graph");
		panel_est.add(btnNewButton);
		
		JButton btnPrintHistogram = new JButton("Print histogram");
		panel_est.add(btnPrintHistogram);
		
		btnNewButton.addActionListener(e -> {
			graphViewIsActive = graphViewIsActive ? false : true;
			graphView.setVisible(graphViewIsActive&&isAreaSelected);
		});

		btnPrintHistogram.addActionListener(e -> {
			histViewIsActive = histViewIsActive ? false : true;
			histView.setVisible(histViewIsActive&&isLatitudeDrawn);
		});
		
		
		
		// SouthPanel
		JPanel panel_south = new JPanel();
		frame.getContentPane().add(panel_south, BorderLayout.SOUTH);
		panel_south.setLayout(new GridLayout(2, 1, 0, 5));
		
		JPanel panel_1 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_1.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		panel_south.add(panel_1);
		
		JLabel lblNewLabel = new JLabel("Year :");
		panel_1.add(lblNewLabel);
		
		txtYeartextfield = new JTextField();
		panel_1.add(txtYeartextfield);
		txtYeartextfield.setText("1880");
		txtYeartextfield.setColumns(10);
		
		txtYeartextfield.addActionListener(e ->{
			controlManager.update_year(Integer.parseInt(txtYeartextfield.getText()));
		});
		
		
		years_slider = new JSlider();
		years_slider.setMajorTickSpacing(20);
		years_slider.setMinorTickSpacing(1);
		years_slider.setPaintLabels(true);
		years_slider.setPaintTicks(true);
		years_slider.setValue(1980);
		years_slider.setMaximum(2020);
		years_slider.setMinimum(1880);
		panel_south.add(years_slider);
		
		years_slider.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				// TODO Auto-generated method stub
				int year = ((JSlider) e.getSource()).getValue();
				controlManager.update_year(year);
			}
		});
		
		controlManager.update_year(1980);
		
		
		frame.pack();
	}
	
	public void update(int current_year) {
		// Adjusting the slider & text field
		txtYeartextfield.setText(Integer.toString(current_year));
		years_slider.setValue(current_year);
	}

	public MapPanel getMapPanel() {
		// TODO Auto-generated method stub
		return panel;
	}
	
	public ControlManager getControlManager() {return controlManager;}
	
	public void setMode(SelectModeManager m) {
		current_mode = m;
		//frame.setTitle("Data Vis'" + current_mode.getMode());
		
	}

	public void setAreaSelected(boolean b){isAreaSelected = b;}
	public void setLatitudeDrawn(boolean b){isLatitudeDrawn = b;}

	public boolean getLatitudeDrawn(){return isLatitudeDrawn;}

	public GraphView getGraphView(){return graphView;}

	public HistView getHistView() {
		return histView;
	}
}
