package presentation;

import javax.swing.*;

import control.ControlManager;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

public class GraphView extends JDialog{
	
	private GraphPanel g_panel;
	private MainFrame main_frame;
	private ControlManager manager;
	private JDialog sa;

	private int currentLat;
	private int currentLon;
	private int currentYear;

	public GraphView(MainFrame main_frame) {
		super();
		init(main_frame);

	}

	private void init(MainFrame main_frame){
		this.setTitle("Graph View");
		this.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);

		this.main_frame = main_frame;
		manager = main_frame.getControlManager();

		float[] extrema = manager.getExtrema();

		g_panel = new GraphPanel(1880, 2020, extrema[0], extrema[1]);
		this.add(g_panel);

		g_panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Point p = new Point(e.getX(),e.getY());
				int year = g_panel.getXValueOnGraph(p);
				manager.update_year(year);
			}
		});
	}

	public void updateGraphData(List<Float> data){
		g_panel.updateData(data);
	}

	public void drawVerticalLine(int year){
		g_panel.drawVerticalLine(year);
	}

	public void updateTitle(){this.setTitle("Graph View - lon = " + currentLon + " lat = " + currentLat + " - selected Year = " + currentYear);}

	public void setCurrentLat(int lat){currentLat = lat;}
	public void setCurrentLon(int lon){currentLon = lon;}
	public void setCurrentYear(int year){currentYear = year;}
}
