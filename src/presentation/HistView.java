package presentation;

import control.ControlManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

public class HistView extends JDialog {

    private MainFrame main_frame;
    private ControlManager manager;
    private HistoPanel h_panel;

    private int currentLat;
    private int currentYear;
    private int currentLon;

    public HistView(MainFrame main_frame){
        super();
        initialize(main_frame);
    }

    private void initialize(MainFrame main_frame){
        this.setTitle("Hist View");
        this.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);

        this.main_frame = main_frame;
        manager = main_frame.getControlManager();

        float[] extrema = manager.getExtrema();

        h_panel = new HistoPanel(-180,180,extrema[0],extrema[1]);
        this.add(h_panel);

        h_panel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                h_panel.deselectBar();
                Point p = new Point(e.getX(),e.getY());
                int lon = h_panel.getBarOnHisto(p);
                h_panel.selectBar(lon);
                manager.update_area(currentLat,lon);
            }
        });


    }

    public void updateTitle(){this.setTitle("Hist View - lat = " + currentLat + " year = " + currentYear + " - selected lon = "+currentLon);}
    public void updateHistData(List<Float> data) {h_panel.updateData(data);}

    public void setCurrentLat(int currentLat) {
        this.currentLat = currentLat;
    }

    public void setCurrentYear(int currentYear) {
        this.currentYear = currentYear;
    }

    public int getCurrentYear(){return currentYear;}

    public int getCurrentLat() {return currentLat;}
}
