package abstraction;

import java.util.HashMap;
import java.util.Map;

/**
 * DataManager stores the temperature anomalies in a data
 * structure which allows to quickly access to the data
 * 
 * @author Cédric Fleury
 * Date : 01/10/2021
 */
public class DataManager {
	
	private Map<Integer, YearTempData> data;
	private int firstYear; 

	/**
	 * Create a Data Manager
	 */
	public DataManager() {
		data = new HashMap<Integer, YearTempData>();
		firstYear = 0;
	}
	
	/**
	 * Instantiate the data value for an additional year
	 * @param year the value of the year
	 */
	public void addYear(int year) {
		data.put(year, new YearTempData(year));
	}
	
	/**
	 * Add a data sample in the data structure
	 * @param tempData the new data sample
	 */
	public void addDataSample(TempData tempData) {
		data.get(tempData.year).addDataSample(tempData);
	}
	
	/**
	 * Define which year is the first year
	 * @param year the value of the year
	 */
	public void setFirstYear(int year) {
		firstYear = year;
	}
	
	/**
	 * Get the first year of the data
	 * @return the first year of the data 
	 */
	public int getFirstYear() {
		return firstYear;
	}
	
	/**
	 * Get the number of years available in data
	 * @return The number of years
	 */
	public int getNumberOfYear() {
		return data.size();
	}
	
	/**
	 * Get a value of a temperature anomaly
	 * @param year the year of the temperature anomaly
	 * @param lat the latitude of the temperature anomaly
	 * @param lon the longitude of the temperature anomaly
	 * @return the temperature anomaly in degree
	 */
	public float getDataSample(int year, int lat, int lon) {
		return data.get(year).getDataSample(lat, lon).getValue();
	}
	
	/**
	 * Get all the temperature anomalies for a given latitude (at a specific year).
	 * @param year the year of the temperature anomalies
	 * @param lat the latitude of the temperature anomalies
	 * @return a map with the longitude as keys and temperature anomalies as values
	 */
	public Map<Integer, Float> getDataByLatitude(int year, int lat) {
		Map<Integer, Float> map = new HashMap<Integer, Float>();
		Map<Integer, TempData> LatData = data.get(year).getLatitudeData(lat).getAllData();
		LatData.keySet().forEach(k -> map.put(k, LatData.get(k).getValue()));
		return map;
	}
	
	/**
	 * Get all the temperature anomalies for a given year
	 * @param year the year of the temperature anomalies
	 * @return a map of maps with the latitude and the longitude as keys and temperature anomalies as values
	 */
	public Map<Integer, Map<Integer, Float>> getDataByYear(int year) {
		Map<Integer, Map<Integer, Float>> map = new HashMap<Integer, Map<Integer, Float>>();
		Map<Integer, LatTempData> yearData = data.get(year).getAllData();
		yearData.keySet().forEach(lat -> map.put(lat, getDataByLatitude(year, lat)));
		return map;
	}
	
	/**
	 * Get the data for all years for a specific area
	 * @param lat the latitude of the area
	 * @param lon the longitude of the area
	 * @return a map with the years as keys and temperature anomalies as values
	 */
	public Map<Integer, Float> getDataForAllYears(int lat, int lon) {
		Map<Integer, Float> map = new HashMap<Integer, Float>();
		data.keySet().forEach(y -> map.put(y, data.get(y).getDataSample(lat, lon).getValue()));
		return map;
	}

	
}
