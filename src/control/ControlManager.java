package control;

import java.awt.Color;
import java.net.URISyntaxException;
import java.sql.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import abstraction.DataManager;
import presentation.GraphView;
import presentation.HistView;
import presentation.MainFrame;
import presentation.MapPanel;


public class ControlManager {
	
	private static final int REDUCE_DELTA = 4;
	private static final int ALPHA_VALUE = 100;
	
	private float[] extrema; // format is [min,max]
	private DataManager manager;
	private MainFrame main_frame;
	
	public ControlManager(MainFrame main_frame) {
		manager = new DataManager();
		this.main_frame = main_frame;
	}
	
	public void initialize_data() {
		Loader loader = new Loader(manager);
		try {
			loader.setUpManager();			
		}catch (Exception e) {
			System.out.println("the manager failed to set up.");
		}
		try {
			loader.loadData(this.getClass().getResource("tempanomaly_4x4grid.csv").toURI().getPath());
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.out.println("le fichier n'est pas trouvée");
		}
		extrema = loader.getExtrema();
	}
	/**
	 * This method update the map & the slider once a year is selected. This method must be called at initialization to display colors.
	 * @param year
	 */
	public void update_year(int year) {
		// Slider & TextField
		main_frame.update(year);

		//TransparentPanel
		Map<Integer, Map<Integer, Float>> data = manager.getDataByYear(year);
		data.keySet().forEach(lat -> {
			data.get(lat).keySet().forEach(lon -> {
				float anomalyValue = data.get(lat).get(lon);
				Color associatedColor = temp2Color(anomalyValue);
				main_frame.getMapPanel().changTileColor(lat,lon,associatedColor);
		});});
		main_frame.getMapPanel().repaint();

		//Vertical Line for GraphView
		main_frame.getGraphView().setCurrentYear(year);
		main_frame.getGraphView().updateTitle();
		main_frame.getGraphView().drawVerticalLine(year);

		// Update histView
		main_frame.getHistView().setCurrentYear(year);
		main_frame.getHistView().updateTitle();
		if(main_frame.getLatitudeDrawn()) {
			this.update_hist_data(main_frame.getHistView().getCurrentLat(),year);
		}

	}

	/**
	 * This method updates the data in the graph. It also updates the title of the JDialog according to the tile selected
	 * @param lat
	 * @param lon
	 */
	public void update_graph_data(int lat, int lon){
		GraphView graphView = main_frame.getGraphView();
		graphView.setCurrentLat(lat);
		graphView.setCurrentLon(lon);
		graphView.updateTitle();
		Map<Integer,Float> data = manager.getDataForAllYears(lat,lon); // We get the data
		List<Float> temperatures_perYear = new ArrayList<Float>();
		data.keySet().forEach(year -> temperatures_perYear.add(data.get(year))); // We add it to a List<Float> for the graphPanel function
		graphView.updateGraphData(temperatures_perYear); // We tell the graphView to update its data.
	}

	/**
	 * This method update the data on the histogram
	 * @param lat
	 * @param year
	 */
	public void update_hist_data(int lat, int year){
		HistView histView = main_frame.getHistView();
		histView.setCurrentLat(lat);
		histView.setCurrentYear(year);
		histView.updateTitle();
		Map<Integer,Float> data = manager.getDataByLatitude(year,lat);
		List<Float> temperature_perLon = new ArrayList<Float>();
		data.keySet().forEach(lon -> {
			temperature_perLon.add(data.get(lon));
		});
		histView.updateHistData(temperature_perLon);
	}

	public void update_area(int lat, int lon){
		main_frame.setAreaSelected(true);
		MapPanel mapPanel = main_frame.getMapPanel();
		mapPanel.selectArea(lat,lon);
		mapPanel.repaint();

		//GraphPanel Aspect
		this.update_graph_data(lat,lon);
	}

	/**
	 * This method update the line that display the selected latitude every time the moused is clicked on the map
	 *
	 */
	public void updateLine(int lat) {
		// TODO Auto-generated method stub
		
	}
	
	private Color temp2Color (float temp) {
		int r = 0, g = 0, b = 0;
		Color newColor;
		if (Float.isNaN(temp)) {
			newColor = new Color(0, 0, 0, 0);
		} else {
			float minValue = extrema[0];
			float maxValue = extrema[1];
			float delta = maxValue - minValue - REDUCE_DELTA;
			float percentage = Math.min((temp - minValue) * 100 / delta, 100);
			
			if(percentage < 33) {
				r = 0;
				g = (int) (7.72 * percentage);
				b = 255;
			} else if (percentage < 66) {
				r = (int) (7.72 * (percentage-33));
				g = 255;
				b = (int) (750 - 7.5 * (percentage+33));
			} else {
				r = 255;
				g = (int) (750 - 7.5 * percentage);
				b = 0;
			}
			newColor = new Color(r, g, b, ALPHA_VALUE);
		}
		return newColor;
	}
	
	public float[] getExtrema() {
		return extrema;
	}
	
}
