package control;

import java.io.BufferedReader;
import java.io.FileReader;

import abstraction.DataManager;
import abstraction.TempData;

public class Loader {
	private DataManager manager;
	
	private float maxValue = Float.NEGATIVE_INFINITY;
	private float minValue = Float.POSITIVE_INFINITY;
	
	public Loader(DataManager manager) {
		this.manager = manager;
	}
	
	public void setUpManager() {
		// Add all the needed years to the data structure
		for (int year = 1880; year < 2021; year++) {
			manager.addYear(year);
		}
		manager.setFirstYear(1880);
	}
	
	public void loadData(String filePath) {
		
		try {
			FileReader file = new FileReader(filePath);
			BufferedReader bufRead = new BufferedReader(file);
			String line = bufRead.readLine();
			
			// Parse the first line
			String[] array = line.split(",");
			
			// Move to the second line
			line = bufRead.readLine();
			while (line != null) {
				// Parse the line
				String[] array1 = line.split(",");
				int lat = Integer.parseInt(array1[0]);
				int lon = Integer.parseInt(array1[1]);
				
				//Go through all the year associated to the given (lat,lon) and add it to the dataManager.
				for (int year_index = 2; year_index < array1.length; year_index++) {
					boolean isDefined;
					float value; 
					if(array1[year_index].equals("NA")) {
						isDefined = false;
						value = 0; // random value, won't 
					}else {
						isDefined = true;
						value = Float.parseFloat(array1[year_index]);
					} 
					TempData t_data = new TempData(lat,lon, 1880+year_index-2,isDefined,value);
					manager.addDataSample(t_data);
					minValue = Math.min(minValue, value);
					maxValue = Math.max(maxValue, value);
				}

				
				line = bufRead.readLine();
			}
			bufRead.close();
			file.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public float[] getExtrema() {
		float [] extrema = new float[2];
		extrema[0] = minValue;
		extrema[1] = maxValue;
		return extrema;
		
	}
	
}
